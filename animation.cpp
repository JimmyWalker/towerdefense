#include "animation.h"

Animation::Animation(QGraphicsItem *parent, int x, int y, int level, QString paths): QObject(), QGraphicsPixmapItem(parent)
{
    this->pos_x = x;
    this->pos_y = y;
    this->level = level;
    this->paths = paths;
    this->sprites = sprites;

    init();
}

Animation::~Animation()
{

}

void Animation::init()
{
    // Setzen von Position und Grafikebene
    setPos(pos_x * 75,pos_y * 75);
    setZValue(level);

    // Alle durch "|" separierten Pfade (nach Auslesen XML Dok) werden in einer Liste zur
    // weiteren Verarbeitung gespeichert
    QStringList pathStrings = paths.split("|", QString::SkipEmptyParts);

    if(pathStrings.count() > 0)
    {
        foreach (QString path, pathStrings)
        {
            // Hinzufügen zum Spritecontainer
            sprites.append(QPixmap(path));
        }
        // setzen des 1. Sprites
        setPixmap(sprites[0]);
    }
}

void Animation::move()
{
    // Sprites austauschen
    setPixmap(sprites[0]);
    sprites.append(sprites[0]);
    sprites.removeAt(0);
}
