#include "waypoint.h"

using namespace std;
// Zugriff auf externe Konstanten
extern const int TILE_SIZE;

Waypoint::Waypoint(QGraphicsItem *parent, int x, int y, int level, QString path, QString direction, int numberOfNext): QObject(), QGraphicsPixmapItem(parent)
{
    this->pos_x = x;
    this->pos_y = y;
    this->level = level;
    this->path = path;
    this->direction = direction;
    this->numberOfNext = numberOfNext;

    init();
}

Waypoint::~Waypoint()
{

}

QString Waypoint::get_direction()
{
    return direction;
}

void Waypoint::init()
{
    setPixmap(QPixmap(path));
    setPos(pos_x * TILE_SIZE,pos_y * TILE_SIZE);
    setZValue(level);
}

QPoint Waypoint::get_centered_point()
{
       int center_x = pos().x() + (pixmap().width() / 2);
       int center_y = pos().y() + (pixmap().height() / 2);
       return QPoint(center_x,center_y);
}

