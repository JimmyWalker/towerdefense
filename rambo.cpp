#include <QPoint>

#include "rambo.h"
#include "gamedata.h"
#include "game.h"

// Zugriff auf globale Game-Daten
extern Gamedata *game_data;
extern Game *game;

// Zugriff auf externe Konstanten
extern const int RAMBO_HEALTH;
extern const int RAMBO_SPEED;
extern const int ENEMY_FROZEN_TIME;


Rambo::Rambo()
{
    health = RAMBO_HEALTH;
    speed = RAMBO_SPEED;
    strength = 25;

    init();
}

Rambo::~Rambo()
{

}

void Rambo::init()
{
    // Animationsbilder laden
    sprites_left.append(QPixmap(":/enemy_sprites/images/enemy_sprites/rambo/left/left1.png"));
    sprites_left.append(QPixmap(":/enemy_sprites/images/enemy_sprites/rambo/left/left2.png"));
    sprites_left.append(QPixmap(":/enemy_sprites/images/enemy_sprites/rambo/left/left3.png"));
    sprites_left.append(QPixmap(":/enemy_sprites/images/enemy_sprites/rambo/left/left4.png"));
    sprites_left.append(QPixmap(":/enemy_sprites/images/enemy_sprites/rambo/left/left5.png"));
    sprites_left.append(QPixmap(":/enemy_sprites/images/enemy_sprites/rambo/left/left6.png"));
    sprites_left.append(QPixmap(":/enemy_sprites/images/enemy_sprites/rambo/left/left7.png"));

    sprites_right.append(QPixmap(":/enemy_sprites/images/enemy_sprites/rambo/right/right1.png"));
    sprites_right.append(QPixmap(":/enemy_sprites/images/enemy_sprites/rambo/right/right2.png"));
    sprites_right.append(QPixmap(":/enemy_sprites/images/enemy_sprites/rambo/right/right3.png"));
    sprites_right.append(QPixmap(":/enemy_sprites/images/enemy_sprites/rambo/right/right4.png"));
    sprites_right.append(QPixmap(":/enemy_sprites/images/enemy_sprites/rambo/right/right5.png"));
    sprites_right.append(QPixmap(":/enemy_sprites/images/enemy_sprites/rambo/right/right6.png"));
    sprites_right.append(QPixmap(":/enemy_sprites/images/enemy_sprites/rambo/right/right7.png"));

    sprites_up.append(QPixmap(":/enemy_sprites/images/enemy_sprites/rambo/up/up1.png"));
    sprites_up.append(QPixmap(":/enemy_sprites/images/enemy_sprites/rambo/up/up2.png"));
    sprites_up.append(QPixmap(":/enemy_sprites/images/enemy_sprites/rambo/up/up3.png"));
    sprites_up.append(QPixmap(":/enemy_sprites/images/enemy_sprites/rambo/up/up4.png"));
    sprites_up.append(QPixmap(":/enemy_sprites/images/enemy_sprites/rambo/up/up5.png"));
    sprites_up.append(QPixmap(":/enemy_sprites/images/enemy_sprites/rambo/up/up6.png"));
    sprites_up.append(QPixmap(":/enemy_sprites/images/enemy_sprites/rambo/up/up7.png"));

    sprites_down.append(QPixmap(":/enemy_sprites/images/enemy_sprites/rambo/down/down1.png"));
    sprites_down.append(QPixmap(":/enemy_sprites/images/enemy_sprites/rambo/down/down2.png"));
    sprites_down.append(QPixmap(":/enemy_sprites/images/enemy_sprites/rambo/down/down3.png"));
    sprites_down.append(QPixmap(":/enemy_sprites/images/enemy_sprites/rambo/down/down4.png"));
    sprites_down.append(QPixmap(":/enemy_sprites/images/enemy_sprites/rambo/down/down5.png"));
    sprites_down.append(QPixmap(":/enemy_sprites/images/enemy_sprites/rambo/down/down6.png"));
    sprites_down.append(QPixmap(":/enemy_sprites/images/enemy_sprites/rambo/down/down7.png"));

    if(direction == "left")setPixmap(sprites_left[0]);
    if(direction == "right")setPixmap(sprites_right[0]);
    if(direction == "up")setPixmap(sprites_up[0]);
    if(direction == "down")setPixmap(sprites_down[0]);

    setZValue(1);
    set_start_position();
}

void Rambo::move()
{

    int current_time = QDateTime::currentMSecsSinceEpoch();

    if( frozen && (current_time - start_frozen) > ENEMY_FROZEN_TIME ) remove_freeze();

    if(direction == "left")
    {
        setPixmap(sprites_left[0]);
        sprites_left.append(sprites_left[0]);
        sprites_left.removeAt(0);

        setPos(x()-speed,y());

        // ist die Hälfte des Gegnerobjektes über oder gleich dem Mittelpunkt des nächsten Wegpunktes (gemessen an der X-Achse),
        // wenn ja, Richtungswechsel in Ausrichtung des passierten Wegpunktes
        if( more_waypoints() && (pos().x()) <= game->list_waypoints[passed_waypoints + 1]->get_centered_point().x() )
        {
            pass_waypoint();
        }

        if( !more_waypoints() && (pos().x() - (pixmap().width() )) < 0)
        {
            finisehd = true;
        }
    }
    if(direction == "right")
    {
        setPixmap(sprites_right[0]);
        sprites_right.append(sprites_right[0]);
        sprites_right.removeAt(0);

        setPos(x()+speed,y());

        // ist die Hälfte des Gegnerobjektes über oder gleich dem Mittelpunkt des nächsten Wegpunktes (gemessen an der X-Achse),
        // wenn ja, Richtungswechsel in Ausrichtung des passierten Wegpunktes
        if( more_waypoints() && (pos().x() + (pixmap().width() / 2)) >= game->list_waypoints[passed_waypoints + 1]->get_centered_point().x() )
        {
            pass_waypoint();
        }

        if( !more_waypoints() && (pos().x()) > 1200)
        {
            finisehd = true;
        }
    }
    if(direction == "up")
    {
        setPixmap(sprites_up[0]);
        sprites_up.append(sprites_up[0]);
        sprites_up.removeAt(0);

        setPos(x(),y()-speed);

        // ist die Hälfte des Gegnerobjektes über oder gleich dem Mittelpunkt des nächsten Wegpunktes (gemessen an der Y-Achse),
        // wenn ja, Richtungswechsel in Ausrichtung des passierten Wegpunktes
        if( more_waypoints() && (pos().y() + (pixmap().height() / 2)) <= game->list_waypoints[passed_waypoints + 1]->get_centered_point().y() )
        {
            pass_waypoint();
        }
        if( !more_waypoints() && (pos().y()) < 0) finisehd = true;
    }

    if(direction == "down")
    {
        setPixmap(sprites_down[0]);
        sprites_down.append(sprites_down[0]);
        sprites_down.removeAt(0);
        setPos(x(),y()+speed);

        // ist die Hälfte des Gegnerobjektes über oder gleich dem Mittelpunkt des nächsten Wegpunktes (gemessen an der Y-Achse),
        // wenn ja, Richtungswechsel in Ausrichtung des passierten Wegpunktes
        if( more_waypoints() && (pos().y() + (pixmap().height() / 2)) >= game->list_waypoints[passed_waypoints + 1]->get_centered_point().y() )
        {
           pass_waypoint();
        }
        if( !more_waypoints() && (pos().y()) > 750) finisehd = true;
    }
}














