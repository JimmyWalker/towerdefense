#ifndef ANIMATION_H
#define ANIMATION_H

#include <QGraphicsScene>
#include <QDebug>
#include <QDateTime>
#include <QTimer>

#include <QString>
#include <QPixmap>
#include <QGraphicsPixmapItem>
#include <QObject>
#include <QGraphicsItem>


/*!
 * \brief Klasse Animation: Wird für Einsatz von Spriteanimationen verwendet.
 * Sie findet im Gameprojekt "Towerdefense" bei Rauchanimation des Industriegebäudes einsatz.
 */
class Animation: public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT

public:
    /*!
    * \brief Animation - Konstruktor
    * \param x         - X Position der Animation
    * \param y         - Y Position der Animation
    * \param level     - Ist die Grafikebene (vergleichbar wie z-index im Bereich Cascading Stylesheets)
    * \param paths     - Datepfadstruktur zu den Sprites
    */
   Animation(QGraphicsItem *parent=0, int x = 0, int y = 0, int level = 0, QString paths = "");

   // Destruktor
   ~Animation();

   /*!
    * \brief move - wird zum wechseln der einzelen Sprites benötigt
    */
   void move();

private:
   int pos_x;
   int pos_y;
   int level;
   // Container in dem die Animationsbilder hinterlegt werden
   QList<QPixmap> sprites;
   QString paths;

   /*!
    * \brief init - Methode zur Initialisierung wichtiger Werte
    */
   void init();


};

#endif // ANIMATION_H
