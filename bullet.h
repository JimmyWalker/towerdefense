#ifndef BULLET_H
#define BULLET_H


#include <QString>
#include <QPixmap>
#include <QGraphicsPixmapItem>
#include <QObject>
#include <QGraphicsItem>
#include <QMediaPlayer>
#include <QGraphicsSceneMouseEvent>
#include <QPoint>


/*!
 * \brief
 */
class Bullet: public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT

public:
   Bullet(QGraphicsItem *parent=0);
   ~Bullet();


   QPixmap img_ice;
   QPixmap img_bullet;
   QPixmap img_rocket;

   QPoint get_centered_point();

   // befindet sich der Turm in der Bauphase?
   bool build_process_active;
   // Methoden die den Bauprozess auslösen und stoppen
   void start_building(QString kind_of_tower);
   void stop_building();
   // Sorgt für die eigentliche Bauanimation
   void building_animation();

private:


};

#endif // BULLET_H
