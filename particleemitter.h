#ifndef PARTICLEEMITTER_H
#define PARTICLEEMITTER_H

#include <QTimer>
#include <QtCore>

#include "particle.h"

/*!
 * \brief Die ParticleEmitter Klasse stellt dem Spiel ein Partikelsystem zur Verfügung.
 * Dies kommt zur Anwendung, wenn der Rocket-Tower seine Munition abfeuert und diese
 * mit den Zielkoordinaten kollidiert.
 */
class ParticleEmitter : public QObject
{
    Q_OBJECT
public:
    ParticleEmitter(QPoint location);
    ~ParticleEmitter();







private:
    // enthält die Anzahl der geforderten Partikelwellen
    int wave;
    // Zeit in der Partikel erzeugt werden
    int time;
    // erfasst die den Unixtimestamp der letzten Partikelwelle
    int last_emitting_time;

    // Position des Partikelsystems
    QPoint location;

    // Container der die die abgesonderten Partikel enthält
    QList<Particle *> list_particles;

    // Timer der notwendig ist um für die Zeit X Partikel abzustoßen
    QTimer *emit_timer;

    inline void emit_particle();

    inline void move_particle();

    inline void check_particleLife();

public slots:
    void update();


};

#endif // PARTICLEEMITTER_H
