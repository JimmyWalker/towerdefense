#ifndef LOGIC_H
#define LOGIC_H

#include <QObject>


/*!
 * \brief Die Logik-Klasse soll das Verhalten der Gegner steuern.
 *        Sie legt u.a. fest welche ob ein Gegner weiter läuft oder
 *        einen Turm attackiert (Noch nicht implementiert)
 */
class Logic: public QObject
{
    Q_OBJECT

public:
   Logic();


private:

public slots:
    void chooseLogic();            // Wählt die Logik für die Gegner
};

#endif // LOGIC_H
