#include "shot.h"
#include "game.h"

extern Game *game;

// Zugriff auf externe konstante Werte
extern const int BULLET_VELOCITY;
extern const int BULLET_DEMAGE;
extern const int BULLET_RADIUS;

extern const int ICE_VELOCITY;
extern const int ICE_DEMAGE;
extern const int ICE_RADIUS;

extern const int ROCKET_VELOCITY;
extern const int ROCKET_DEMAGE;
extern const int ROCKET_RADIUS;

Shot::Shot(QGraphicsItem *parent, QPoint start, QPoint aim, QString kind)
{
    this->setPos(start);
    this->kind_of_shot = kind;
    this->aim = aim;

    init();
}

Shot::~Shot()
{
    game->scene->removeItem(this);
    game->list_shots.removeOne(this);
}

void Shot::init()
{
    /*
     * Festlegen der Eigenschaften und Erscheinung
     */
    if ( kind_of_shot == "bullet")
    {
        setPixmap(QPixmap(":/shots/images/shots/bullet_shot.png"));
        velocity = BULLET_VELOCITY;
        demage = BULLET_DEMAGE;
        demage_radius = BULLET_RADIUS;
    }

    if ( kind_of_shot == "ice")
    {
        setPixmap(QPixmap(":/shots/images/shots/ice_shot.png"));
        velocity = ICE_VELOCITY;
        demage = BULLET_DEMAGE;
        demage_radius = BULLET_RADIUS;
    }

    if ( kind_of_shot == "rocket")
    {
        setPixmap(QPixmap(":/shots/images/shots/rocket_shot.png"));
        velocity = ROCKET_VELOCITY;
        demage = ROCKET_DEMAGE;
        demage_radius = ROCKET_RADIUS;
    }

    setZValue(1);
}

void Shot::move()
{
    /*
     * Berechnung der neuen Position unter Berücksichtigung
     * der Geschwindigkeit.
     */
    double theta = rotation();

    double dy = velocity * qSin(qDegreesToRadians(theta));
    double dx = velocity * qCos(qDegreesToRadians(theta));

    setPos(x()+dx,y()+dy);

    // Kollision mit angepeiltem Ziel prüfen
    if( x()+pixmap().width()/2 <= aim.x()+10
            && x()+pixmap().width()/2 >= aim.x()
            && y()+pixmap().height()/2 > aim.y()
            && y()+pixmap().height()/2 <= aim.y()+10 )
    {
        check_demage();
        // Ausschließlich für Raketen wird das Partikelsystem angewandt
        if(kind_of_shot == "rocket")game->create_particle_emitter(aim);
    }

}

void Shot::check_demage()
{
    for (int i = 0; i < game->list_enemies.count(); i++)
    {
        // Explosion im Umfeld eines Gegners?
        if( game->list_enemies[i]->get_centered_point().x() <= x()+demage_radius
            && game->list_enemies[i]->get_centered_point().x() >= x()-demage_radius
            && game->list_enemies[i]->get_centered_point().y() >= y()-demage_radius
            && game->list_enemies[i]->get_centered_point().y() <= y()+demage_radius)
        {
            game->list_enemies[i]->decrease_health(demage);

            if(kind_of_shot == "ice" && game->list_enemies[i]->frozen == false)game->list_enemies[i]->freeze_speed();
        }
    }
    // Entfernt das Schussobjekt
    delete this;
}

/* double Shot::get_distance()
{
  return sqrt( pow(this->aim.x() - this->x(),2 ) + pow( this->aim.y() - this->y(),2 ) );
} */

