#ifndef ENEMY_H
#define ENEMY_H

#include <QDateTime>
#include <QTimer>
#include <QGraphicsScene>
#include <QList>
#include <stdlib.h>
#include <typeinfo>
#include <QDebug>
#include <QGraphicsPixmapItem>
#include <QGraphicsItem>
#include <QObject>
#include <QList>

/*!
 * \brief Die Enemy Klasse ist die Oberklasse der Gegner, von
 *  ihr erben alle speziellen Gegner (in dieser Implementierung die Klasse "Rambo")
 */
class Enemy: public QObject, public QGraphicsPixmapItem
{
    // Signals and Slots möglich machen
    Q_OBJECT

public:
   // Konstruktor
   Enemy(QGraphicsItem * parent=0);
   // Destruktor löscht das Objekt aus einer zentralen Gameobjektliste
   ~Enemy();

   // Lebensenergie
   int health;

   /*
    *  Stärke des Gegners (Wurde in dieser Implementierungsphase nicht brücksichtigt)
    *  Den Gegnern soll es später ermöglicht werden Türme zu beschädigen
    */
   int strength;

   // Bewegungsgeschwindigkeit
   int speed;

   // Gegner eingefroren? (Durch "Ice-Tower", dieser verlangsamt die Gegner)
   bool frozen;

   // Richtung in die sich der Gegner bewegt
   QString direction;

   // merkt sich wieviele Waypoints bereits zurückgelegt wurden
   int passed_waypoints;

   // Hat gegner End-Wegpunkt überschritten
   bool finisehd;

   /*!
    * \brief more_waypoints - ermittelt ob es noch mehr Wegpunkte zum zurücklegen gibt
    * \return - true, wenn es noch passierbare gibt
    */
   bool more_waypoints();

   /*!
    * \brief pass_waypoint - setzt die Anzahl der absolvierten Waypoint hoch
    *  und gibt dem Gegner die nächste Richtung in die er sich bewegen muss
    */
   void pass_waypoint();

   // friert den Gegner ein
   void freeze_speed();

   // löst den verlangsamten Zustand wieder auf
   void remove_freeze();

   // Wann wurde der Gegner eingefroren (Nach Zeitspanne X wird der Gegner wieder aufgetaut)
   int start_frozen;

   // gibt den Mittelpunkt der Grafik des Gegners zurück
   QPoint get_centered_point();

   /*!
   * \brief set_start_position
   *  Ermittelt den Mittelpunkt des ersten Waypoint und
    * richtet das aktuelle Sprite mittig aus.
   */
  void set_start_position();

   // Ermöglicht das Bewegen innerhalb des Gameloops
  virtual void move();

   // der Gegner feuert Munition ab
  virtual void shoot();

   /*!
    * \brief get_health - Gibt die Lebenspunkte des Gegenr zurück
    * \return - int: Anzahl der Lebenspunkte
    */
   int get_health();
   /*!
    * \brief decrease_health - vermindern der Lebenspunkte
    * \param val - int: vermindert die Lebenspunkte des Gegners
    */
   void decrease_health(int &val);

private:
   int get_posx();
   int get_posy();



};
#endif // ENEMY_H
