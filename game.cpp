#include "game.h"

// Zugriff auf externe Konstanten
extern const int GAME_LOOP_TIMER_VAL;
extern const int GAME_FIELD_SIZE;
extern const int TILE_SIZE;


Game::Game(): menuIsActive(false), start_game(false)
{
    // Größe des Fensters bestimmen und Scene setzen
    scene = new QGraphicsScene();
    scene->setSceneRect(0,0,1200,750);
    setScene(scene);

    // weitere Szeneneinstellungen
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setFixedSize(1200,750);

    // Initialisierung wichtig Werte und Erstellung der Map
    init();

    show();
}

void Game::init()
{
    QDomDocument document;

    // XML Datei laden
    QFile file("map_level_01.xml");

    if (!file.open(QIODevice::ReadOnly |  QIODevice::Text ))
    {
        qDebug() << "XML Error :: " << file.error();
    }
    else
    {
        if(!document.setContent(&file)) qDebug() << "Dokument konnte nicht geladen werden";

        file.close();
    }

    QDomElement root = document.firstChildElement();

    create_map(root);

    // Erstellen der Score / Coins - Backgroundgraphic
    coins_bg = new QGraphicsPixmapItem(QPixmap(":/allgemein/images/score.png"));
    coins_bg->setZValue(1);
    scene->addItem(coins_bg);

    // Erstellen der Coins / Score -Zahlen
    score = new Score();
    scene->addItem(score);


    // Erstellen des Gameloop
    gameloop = new Gameloop();
    gameloop_timer = new QTimer();
    connect(gameloop_timer, SIGNAL(timeout()),gameloop,SLOT(update()));
    gameloop_timer->start(GAME_LOOP_TIMER_VAL);

    // Spieler erzeugen
    player = new Player();
}


void Game::create_map( QDomElement &root )
{
    // Alle nötigen Elemente aus der Map holen
    QDomNodeList items_bg = root.elementsByTagName("bg");
    QDomNodeList items_assets = root.elementsByTagName("assets");
    QDomNodeList items_buildingplaces = root.elementsByTagName("buildingPlaces");
    QDomNodeList items_animations = root.elementsByTagName("animations");
    QDomNodeList items_road = root.elementsByTagName("road");

    // Werte initialisieren
    int pos_x = 0;
    int pos_y = 0;
    int level = 0;
    QString path = "";
    QString pathString = "";
    QString direction = "";


    // Durchgehen des item_bg
    QDomNode node_bg = items_bg.at(0);

    // Kovertieren des Elements
    if(node_bg.isElement())
    {
        QDomNodeList bg_elements = node_bg.childNodes();

        for(int e = 0; e < bg_elements.count(); e++)
        {
            if( bg_elements.at(e).nodeName() == "path" ) path = bg_elements.at(e).toElement().text();

            if( bg_elements.at(e).nodeName() == "level" )level= bg_elements.at(e).toElement().text().toInt();

            create_bg(path, level);
        }
    } // Ende BG

    // Durchgehen des item_assets
    QDomNode node_assets = items_assets.at(0);

    // Kovertieren des Elements
    if(node_assets.isElement())
    {

        QDomNodeList assets_elements = node_assets.childNodes();

        for(int e = 0; e < assets_elements.count(); e++)
        {
            QDomNode asset = assets_elements.at(e);
            QDomNodeList values = asset.childNodes();

            for(int n = 0; n < values.count(); n++)
            {

                QDomNode val = values.at(n);
                QDomElement dom_val = val.toElement();

                if( val.nodeName() == "path" ) path =  dom_val.text();
                if( val.nodeName()  == "pos" )
                {
                    QString pos_string = dom_val.text();

                    QStringList xy = pos_string.split(",", QString::SkipEmptyParts);
                    pos_x = xy[0].toInt();
                    pos_y = xy[1].toInt();
                }

                if( val.nodeName()  == "level" ) level = dom_val.text().toInt();
            }
            create_asset(path, pos_x, pos_y, level);
        }
    } // Ende Assets


    // Holen Aller Animationen
    QDomNode nodes_animations = items_animations.at(0);

    // Kovertieren des Elements
    if(nodes_animations.isElement())
    {
        // Holen der einzelnen Animationen
        QDomNodeList animation_singleAnimation = nodes_animations.childNodes();

        for(int e = 0; e < animation_singleAnimation.count(); e++)
        {
            // Abfragen aller Animationswerte
            QDomNode animation_node = animation_singleAnimation.at(e);
            QDomNodeList values = animation_node.childNodes();

            for (int v = 0; v < values.count(); ++v) {
                QDomNode val = values.at(v);
                QDomElement val_element = val.toElement();

                if( val.nodeName() == "pos" )
                {
                    QString posString = val_element.text();
                    QStringList xy = posString.split(",", QString::SkipEmptyParts);

                    pos_x = xy[0].toInt();
                    pos_y = xy[1].toInt();
                }

                if( val.nodeName() == "level" )level = val_element.text().toInt();

                if( val.nodeName() == "paths" )
                {
                    QDomNodeList paths = val.childNodes();

                    for (int p = 0; p < paths.count(); ++p) {

                        QDomNode path_node = paths.at(p);
                        QDomElement path = path_node.toElement();

                        // Pfade zusammenfügeb
                        if(p == 0) pathString += path.text();
                        else pathString += "|" + path.text();
                    }
                }
            }

            Animation *an = new Animation(0,pos_x-1,pos_y-1,level,pathString);
            list_animations.append(an);

            scene->addItem(an);
        }
    } // Ende Animations


    // Durchgehen der Buildingplaces
    QDomNode node_buildingplaces = items_buildingplaces.at(0);

    // Kovertieren des Elements
    if(node_buildingplaces.isElement())
    {
        QDomNodeList nodes_places = node_buildingplaces.childNodes();

        for(int e = 0; e < nodes_places.count(); e++)
        {
            QDomElement element= nodes_places.at(e).toElement();
            if( nodes_places.at(e).nodeName() == "level" ) level = element.text().toInt();
            if( nodes_places.at(e).nodeName() == "path" ) path =  element.text();
            if( nodes_places.at(e).nodeName() == "pos" )
            {
                QString posString = element.text();
                QStringList position_strings = posString.split("|", QString::SkipEmptyParts);

                foreach (QString pos, position_strings) {
                    QStringList xy = pos.split(",", QString::SkipEmptyParts);
                    pos_x = xy[0].toInt();
                    pos_y = xy[1].toInt();

                    create_tower(path, pos_x, pos_y, level);
                }
            }
        }
    } // Ende Tower


    // Durchgehen des item_roads
    QDomNode nodes_road = items_road.at(0);


    // Kovertieren des Elements
    if(nodes_road.isElement())
    {

        QDomNodeList road_elements = nodes_road.childNodes();

        for(int e = 0; e < road_elements.count(); e++)
        {
            QDomNode road = road_elements.at(e);
            QDomNodeList values = road.childNodes();
            int numberOfNext = 0;

            for(int n = 0; n < values.count(); n++)
            {

                QDomNode val = values.at(n);
                QDomElement dom_val = val.toElement();

                if( val.nodeName() == "path" ) path =  dom_val.text();
                if( val.nodeName()  == "pos" )
                {
                    QString pos_string = dom_val.text();

                    QStringList xy = pos_string.split(",", QString::SkipEmptyParts);
                    pos_x = xy[0].toInt();
                    pos_y = xy[1].toInt();
                }

                if( val.nodeName()  == "level" ) level = dom_val.text().toInt();
                if( val.nodeName()  == "direction" )direction = dom_val.text();

                if( val.nodeName()  == "numberOfNext" )
                {
                    QString other_string = dom_val.text();
                    numberOfNext = other_string.toInt();

                    for( int oth = 1; oth <= numberOfNext; oth++ )
                    {
                        if(direction == "left") create_road(path, pos_x  - oth, pos_y, level);
                        if(direction == "right") create_road(path, pos_x  + oth, pos_y, level);
                        if(direction == "up") create_road(path, pos_x, pos_y - oth, level);
                        if(direction == "down") create_road(path, pos_x, pos_y + oth, level);
                    }
                }
            }
            Waypoint *wp = new Waypoint(0,pos_x-1, pos_y-1, level, path, direction, numberOfNext );
            // Straßenteil wird zur Wegpunkt Liste hinzugefügt
            list_waypoints.append(wp);
            /* Straßenteil wird zur Gesamtliste (Straße) hinzugefügt
            *  Geschütztürme richten sich später anhand der Straße aus (Drehreichtung)*/
            list_road.append(wp);

            scene->addItem(wp);
        }
    } // Ende Road
}

void Game::create_bg(QString &path, int &level)
{
    // Das Spielfeld umfasst 16x16 Felder
    for(int i = 0; i < GAME_FIELD_SIZE; i++)
    {
        for(int k = 0; k < GAME_FIELD_SIZE; k++)
        {
            QGraphicsPixmapItem *bg_tile = new QGraphicsPixmapItem();
            bg_tile->setPixmap(QPixmap(path));
            bg_tile->setZValue(level);
            bg_tile->setPos(i * TILE_SIZE,k * TILE_SIZE);
            scene->addItem(bg_tile);
        }
    }
}

void Game::create_asset(QString &path, int &pos_x, int &pos_y, int &level)
{
    QGraphicsPixmapItem *asset = new QGraphicsPixmapItem();
    asset->setPixmap(QPixmap(path));
    asset->setZValue(level);
    asset->setPos((pos_x-1)* TILE_SIZE, (pos_y-1)* TILE_SIZE);
    scene->addItem(asset);
}

void Game::create_tower(QString &path, int &pos_x, int &pos_y, int &level)
{
    Tower *tower = new Tower(0,pos_x-1, pos_y-1, level, path);
    tower->setFlag(QGraphicsItem::ItemIsFocusable);

    list_towers.append(tower);
    scene->addItem(tower);

}

void Game::create_road(QString path, int pos_x, int pos_y, int level)
{
    QGraphicsPixmapItem *asset = new QGraphicsPixmapItem();
    asset->setPixmap(QPixmap(path));
    asset->setZValue(level);
    asset->setPos((pos_x-1) * TILE_SIZE, (pos_y-1) * TILE_SIZE);
    scene->addItem(asset);
    list_road.append(asset);
}


void Game::mousePressEvent(QMouseEvent *mevent)
{
    /***
     * Es wird geprüft ob der Spieler auf einen Turm geklickt hat,
     * wenn ja, so erscheint ihm ein Bau Menür für den Turm */
    if(list_towers.count() > 0)
    {
        for(int i = 0; i < list_towers.count(); i++){

            // Wenn der angeklickte Tower noch nicht bebaut wurde
            if(list_towers[i] == itemAt(mevent->pos()) && list_towers[i]->kind_of_tower == "" )
            {
                if(!menuIsActive)
                {
                   show_menu(i);
                }
                return;
            }
        }
    }
}

void Game::scene_update()
{
    scene->update(sceneRect());
}

void Game::show_menu(int towerIndex)
{
    menuIsActive = true;
    menu = new Buymenu(0,towerIndex);
    menu->setZValue(1);
    scene->addItem(menu);
    scene_update();
}

void Game::hide_menu()
{
    menuIsActive = false;
    scene->removeItem(menu);
    menu = NULL;
}

void Game::set_debugLine(QPoint p1, QPoint p2)
{
    QLineF debugLine( p1, p2 );
    scene->addLine(debugLine);
}

void Game::create_particle_emitter(QPoint loc)
{
    ParticleEmitter *pe = new ParticleEmitter(loc);
    list_emitter.append(pe);
}

