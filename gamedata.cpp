extern const int COINS_TOWER_ROCKET = 230;
extern const int COINS_TOWER_BULLET = 150;
extern const int COINS_TOWER_ICE = 100;


extern const int GAME_LOOP_TIMER_VAL = 20;
extern const int GAME_FIELD_SIZE = 16;
extern const int TILE_SIZE = 75;

extern const int ANIMATION_DELAY_TIME = 150;
extern const int COINS_FOR_ENEMY = 15;

extern const int PARTICLE_START_SIZE = 1;
extern const int PARTICLE_LIFE_SPAN = 255;

extern const int PARTICLE_VELOCITY = 3;
extern const double PARTICLE_ACCELERATION = 0.1;

extern const int START_COINS = 600;

extern const int RAMBO_HEALTH = 100;
extern const int RAMBO_SPEED = 2;

extern const int ENEMY_FROZEN_TIME = 3500;

extern const int BULLET_VELOCITY = 10;
extern const int BULLET_DEMAGE = 4;
extern const int BULLET_RADIUS = 40;

extern const int ICE_VELOCITY = 8;
extern const int ICE_DEMAGE = 0;
extern const int ICE_RADIUS = 100;

extern const int ROCKET_VELOCITY = 6;
extern const int ROCKET_DEMAGE = 20;
extern const int ROCKET_RADIUS = 80;

extern const int ICE_ATTACK_RADIUS = 300;
extern const int ICE_SHOT_RATE = 2500;

extern const int BULLET_ATTACK_RADIUS = 400;
extern const int BULLET_SHOT_RATE = 150;


extern const int ROCKET_ATTACK_RADIUS = 500;
extern const int ROCKET_SHOT_RATE = 4000;




