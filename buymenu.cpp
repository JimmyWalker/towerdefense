#include "buymenu.h"
#include "game.h"


// Zugriff auf globales Gameobjekt
extern Game *game;

// Zugriff auf externe Konstanten
extern const int COINS_TOWER_ICE;
extern const int COINS_TOWER_BULLET;
extern const int COINS_TOWER_ROCKET;

 Buymenu::Buymenu(QGraphicsItem *parent/* = 0*/,int towerIndex)
     : QGraphicsLayoutItem(), QGraphicsItem(parent), active_item("")
 {
    // Id des Towers
    this->towerIndex = towerIndex;

    ring = new QPixmap(QLatin1String(":/buymenu/images/buymenu/ring.png"));

    // Grafiken für Items zuweisen
    ice = new BuyItem("ice",120,":/buymenu/images/buymenu/ice_chosen.png",":/buymenu/images/buymenu/ice_buyable.png", ":/buymenu/images/buymenu/ice_not_enough_coins.png", QPoint(80, 27), game->list_towers[towerIndex]->get_centered_point() );
    bullet = new BuyItem("bullet",120,":/buymenu/images/buymenu/bullet_chosen.png",":/buymenu/images/buymenu/bullet_buyable.png", ":/buymenu/images/buymenu/bullet_not_enough_coins.png", QPoint(23, 82), game->list_towers[towerIndex]->get_centered_point() );
    rocket = new BuyItem("rocket",120,":/buymenu/images/buymenu/rocket_chosen.png",":/buymenu/images/buymenu/rocket_buyable.png", ":/buymenu/images/buymenu/rocket_not_enough_coins.png", QPoint(-33, 27),game->list_towers[towerIndex]->get_centered_point() );
    dollar = new BuyItem("dollar",120,":/buymenu/images/buymenu/buy_active.png",":/buymenu/images/buymenu/buy_inactive.png", ":/buymenu/images/buymenu/buy_inactive.png", QPoint(23, -34),game->list_towers[towerIndex]->get_centered_point() );

    width = ring->width();
    height = ring->height();

    setGraphicsItem(this);

    // Mittpunkt des Towers um die MenüGrafik daran auszurichten
    posx = game->list_towers[towerIndex]->get_centered_point().x();
    posy = game->list_towers[towerIndex]->get_centered_point().y();

    set_icons();
 }

 Buymenu::~Buymenu()
 {

 }

 /*
  * Während das Menü offen ist, werden alle Items wiederholt gezeichnet
  */
 void Buymenu::paint(QPainter *painter,
     const QStyleOptionGraphicsItem *option, QWidget *widget /*= 0*/)
 {
     Q_UNUSED(widget);
     Q_UNUSED(option);

     // Ramen in dem Das Menü gezeichnet wird
     QRectF frame(QPointF(posx,posy), geometry().size());

     // Zeichnen der grafischen Elemente
     QPointF ring_pos = frame.center() - (QPointF(width, height)/2);

     // Zeichnen der Items
     painter->drawPixmap(ring_pos, *ring);
     painter->drawPixmap(ice->get_pos(), *ice);
     painter->drawPixmap(bullet->get_pos(), *bullet);
     painter->drawPixmap(rocket->get_pos(), *rocket);
     painter->drawPixmap(dollar->get_pos(), *dollar);
 }

 QRectF Buymenu::boundingRect() const
 {
     return QRectF(QPointF(posx-(width/2),posy-(height/2)), QSizeF(width,height));
 }

 QSizeF Buymenu::sizeHint(Qt::SizeHint which, const QSizeF &constraint) const
 {
     switch (which) {
     case Qt::MinimumSize:
     case Qt::PreferredSize:
         return ring->size() + QSize(12, 12);
     case Qt::MaximumSize:

     default:
         break;
     }
     return constraint;
 }

 void Buymenu::mousePressEvent( QGraphicsSceneMouseEvent *event )
 {
    QGraphicsItem::mousePressEvent( event );

    if(list_buyable.count() > 0)
    {
        // Auf ICE gedrückt
        if( event->pos().x() >= ice->get_pos().x() && event->pos().x() <= ice->get_pos().x() + ice->width()
         && event->pos().y() >= ice->get_pos().y() && event->pos().y() <= ice->get_pos().y() + ice->height() && list_buyable.contains("ice"))
        {
            set_active("ice");
            return;
        }

        // Auf BULLET gedrückt
        if( event->pos().x() >= bullet->get_pos().x() && event->pos().x() <= bullet->get_pos().x() + bullet->width()
         && event->pos().y() >= bullet->get_pos().y() && event->pos().y() <= bullet->get_pos().y() + bullet->height() && list_buyable.contains("bullet"))
        {
            set_active("bullet");
            return;
        }

        // Auf ROCKET gedrückt
        if( event->pos().x() >= rocket->get_pos().x() && event->pos().x() <= rocket->get_pos().x() + rocket->width()
         && event->pos().y() >= rocket->get_pos().y() && event->pos().y() <= rocket->get_pos().y() + rocket->height() && list_buyable.contains("rocket"))
        {
            set_active("rocket");
            return;

        }

        // Auf Kaufen gedrückt und es ist ein ITEM aktiv,dh. Kaufvorgang wird ausgelöst
        if( event->pos().x() >= dollar->get_pos().x() && event->pos().x() <= dollar->get_pos().x() + dollar->width()
         && event->pos().y() >= dollar->get_pos().y() && event->pos().y() <= dollar->get_pos().y() + dollar->height() && active_item != "")
        {
           buy_action();
        }
    }
    // Drückt man in den Mittelpunkt des Menüs verschwindet es wieder
    game->hide_menu();
 }

 void Buymenu::set_icons()
 {
     if( game->player->get_coins() >= COINS_TOWER_ROCKET )
     {
         // ALLE FREISCHALTEN
         ice->set_item_buyable();
         bullet->set_item_buyable();
         rocket->set_item_buyable();

         list_buyable.append("ice");
         list_buyable.append("bullet");
         list_buyable.append("rocket");
     }

     if( game->player->get_coins() >= COINS_TOWER_ICE && game->player->get_coins() < COINS_TOWER_ROCKET )
     {
         // BULLET & ICE FREISCHALTEN
         rocket->set_item_non_buyable();
         ice->set_item_buyable();
         bullet->set_item_buyable();

         list_buyable.append("ice");
         list_buyable.append("bullet");
     }

     if( game->player->get_coins() < COINS_TOWER_BULLET && game->player->get_coins() >= COINS_TOWER_ICE )
     {
         // ICE FREISCHALTEN
         ice->set_item_buyable();
         bullet->set_item_non_buyable();
         rocket->set_item_non_buyable();

         list_buyable.append("ice");
     }

     if( game->player->get_coins() < COINS_TOWER_ICE )
     {
         // NICHTS FREISCHALTEN
         ice->set_item_non_buyable();
         bullet->set_item_non_buyable();
         rocket->set_item_non_buyable();
     }

     dollar->set_item_non_buyable();
 }

 void Buymenu::set_active(QString name)
 {
    active_item = name;
    // Dollar Item wird für den Kauf aktiv gesetzt
    dollar->set_item_clicked();

    // ICE auswählen
    if( active_item == "ice")
    {
        ice->set_item_clicked();
        if(list_buyable.contains("rocket"))
        {
            bullet->set_item_buyable();
            rocket->set_item_buyable();
        }
        else if(list_buyable.contains("bullet"))
        {
            bullet->set_item_buyable();
        }
    }

    // BULLET auswählen
    if( active_item == "bullet")
    {
        bullet->set_item_clicked();
        ice->set_item_buyable();

        if(list_buyable.contains("rocket"))
        {
            rocket->set_item_buyable();
        }
    }

    // Rocket auswählen
    if( active_item == "rocket")
    {
            rocket->set_item_clicked();
            ice->set_item_buyable();
            bullet->set_item_buyable();
    }
 }

 void Buymenu::buy_action()
 {
    // Dem Spieler werden je nach Item-Kauf entsprechend Credits abgezogen
    if(active_item == "ice")game->player->manipulate_coins( - COINS_TOWER_ICE);
    if(active_item == "bullet")game->player->manipulate_coins( - COINS_TOWER_BULLET);
    if(active_item == "rocket")game->player->manipulate_coins( - COINS_TOWER_ROCKET);

    // Welcher Tower soll bebaut werden und mit welcher Spezialisierung?
    game->list_towers[towerIndex]->start_building(active_item);
 }




