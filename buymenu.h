#ifndef BUYMENU_H
#define BUYMENU_H

#include <QObject>
#include <QGraphicsItem>
#include <QGraphicsLayoutItem>
#include <QtGui>
#include <QPoint>
#include <QMouseEvent>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsView>

#include "buyitem.h"

/*!
 * \brief Die Kaufmenüklasse ermöglicht dem Spieler Geschütztürme auszubauen.
 */
class Buymenu : public QGraphicsLayoutItem, public QGraphicsItem
{
public:
    Buymenu(QGraphicsItem *parent = 0,int towerIndex = 0);
    ~Buymenu();

    // geerbt von QGraphicsLayoutItem
        // Wird genutzt um die empfohlene Darstellung des Widget zu finden
        QSizeF sizeHint(Qt::SizeHint which, const QSizeF &constraint = QSizeF()) const;

    // geerbt von from QGraphicsItem
        // Gibt den klickbaren Bereich des Menüs zurück
        QRectF boundingRect() const;
        //Zeichen Methpde für die Darstellung
        void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
        QWidget *widget = 0);


private:

    // Position des Kaufmenüs
    int posx;
    int posy;

    // Container für Items die der Spieler mit seinen Credits bezahlen kann
    QList<QString> list_buyable;

    // Index des Towers der errichtet werden soll
    int towerIndex;

    // Bild für die Hexagonform des Kaufmenüs
    QPixmap *ring;

    // Breite und Höhe des Menüs
    qreal width;
    qreal height;

    // Clickbare Menüitems
    BuyItem *ice;
    BuyItem *bullet;
    BuyItem *rocket;
    BuyItem *dollar;

    // Position des Kaufzeichens
    QPointF dollar_pos;

    // trägt die Bezeichnung des Items das gerade aktiv ist
    QString active_item;

    // Realisierung der Maussteuerung im Menü
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    // setzt die Item-Grafiken (z.B. inaktiv, kaufbar ...)
    void set_icons();
    /*!
     * \brief set_active - setzt das Item aktiv das angeklickt wurde
     * \param name - Bezeichnung für das angeklickte Item
     */
    void set_active(QString name);

    // Methode, welche die Kaufaktion auslöst
    void buy_action();
};

#endif // BUYMENU_H
