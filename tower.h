#ifndef TOWER_H
#define TOWER_H


#include <QString>
#include <QPixmap>
#include <QGraphicsPixmapItem>
#include <QObject>
#include <QGraphicsItem>
#include <QMediaPlayer>
#include <QGraphicsSceneMouseEvent>
#include <QPoint>
#include <cmath>
#include <QGraphicsScene>
#include <QDebug>
#include <QDateTime>
#include <QTimer>
#include <QMouseEvent>

#include "shot.h"



/*!
 * \brief die Klasse Tower bietet dem Spieler die Möglichkeit Verteidigungsposten
 *  für die gegnerischen Wellen auszubauen. Es gibt 3 unterschiedliche Arten von Türmen
 * (Ice: verlangsamt die Gegner für kurze Zeit, Bullet: schnelles Dauerfeuer mit wenig Schaden,
 * Rocket: hoher Schaden, bei langsamer Feuerrate)
 */
class Tower: public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT

public:
   Tower(QGraphicsItem *parent=0, int x = 0, int y = 0, int level = 0, QString path = "");
   ~Tower();

   QString kind_of_tower;

   QList<QPixmap> sprites_building;

   QPoint get_centered_point();

   // befindet sich der Turm in der Bauphase?
   bool build_process_active;

   int get_pos_x();
   int get_pos_y();

   // Methoden die den Bauprozess auslösen und stoppen
   void start_building(QString kind_of_tower);
   void stop_building();

   // Sorgt für die eigentliche Bauanimation
   void building_animation();
   void scan_attack_radius();

private:
   int pos_x;
   int pos_y;
   int level;
   double attack_radius;
   // aller wie viel ms darf geschossen werden
   int shot_rate;

   // Wert der von der Lebensenergie der Gegner abgezogen wird
   int demage;

   // Wann wurde das letzte mal geschossen?
   int time_last_shot;

   QString path;

   // Zählt wieviel Baubilder bereits abgespielt wurden
   int animation_counter;

   void init();
   // legt fest welche Art von Turm gebaut wird (je nach Kauf)
   void specify_tower();

   // sondert eine Kugel auf ein bestimmtes Ziel
   void shot(QPoint ziel);

   double get_distance(QPoint ziel);

};

#endif // TOWER_H
