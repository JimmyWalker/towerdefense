#include "enemy.h"
#include "game.h"

// Zugriff auf globale Komponenten
extern Game *game;

Enemy::Enemy(QGraphicsItem *parent):
    QObject(), QGraphicsPixmapItem(parent), passed_waypoints(0), finisehd(false), frozen(false), start_frozen(0)
{
    // Setzen der 1. Wegrichtung
    this->direction = game->list_waypoints[0]->get_direction();
}

Enemy::~Enemy()
{
    game->scene->removeItem(this);
    game->list_enemies.removeOne(this);
}

bool Enemy::more_waypoints()
{
    return ( (passed_waypoints + 1) < game->list_waypoints.count() );
}

void Enemy::pass_waypoint()
{
    passed_waypoints += 1;
    direction = game->list_waypoints[passed_waypoints]->get_direction();
}

void Enemy::freeze_speed()
{
    frozen = true;
    start_frozen = QDateTime::currentMSecsSinceEpoch();
    speed -= 1;
}

void Enemy::remove_freeze()
{
    speed += 1;
    frozen = false;
}

void Enemy::set_start_position()
{
    // Holen des Mittelpunktes des Startwegpunktes
    QPoint centeredWaypoint = game->list_waypoints[0]->get_centered_point();

    // Objekt wird ausgerichtet am Mittelpunkt des Startwegpunktes
    int start_x = centeredWaypoint.x() - (pixmap().width() / 2);
    int start_y = centeredWaypoint.y() - (pixmap().height() / 2);

    setPos(start_x, start_y);
}

void Enemy::shoot()
{

}

int Enemy::get_health()
{
    return health;
}

void Enemy::decrease_health(int &val)
{
    health -= val;
}

int Enemy::get_posx()
{
    return pos().x();
}

int Enemy::get_posy()
{
    return pos().y();
}

void Enemy::move()
{

}

QPoint Enemy::get_centered_point()
{
    int center_x = pos().x() + (pixmap().width() / 2);
    int center_y = pos().y() + (pixmap().height() / 2);
    return QPoint(center_x,center_y);
}
