#ifndef RAMBO_H
#define RAMBO_H

#include "enemy.h"

/*!
 * \brief Die Rambo Klasse ist der 1. im Spiel implementierte Gegner und erbt Eigenschaften und Methoden
 * von der Elternklasse "Enemy".
 */
class Rambo : public Enemy
{
public:
    Rambo();
   ~Rambo();

    // Animationsbilder für die Bewegungen
    QList<QPixmap> sprites_left;
    QList<QPixmap> sprites_right;
    QList<QPixmap> sprites_up;
    QList<QPixmap> sprites_down;

    // bewegt den Gegner über das Spielfeld
    void move();

private:
    // Initialisiert wichtige Werte bei der Erstellung des Gegners
    void init();
};

#endif // RAMBO_H
