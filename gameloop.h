#ifndef GAMELOOP_H
#define GAMELOOP_H

#include <QObject>
#include <QGraphicsScene>
#include <QDebug>
#include <QDateTime>

#include "rambo.h"
#include "buymenu.h"

/*!
 * \brief Die Gamingloop-Klasse realisiert den Kern der Bewegungsabläufe des Spiels.
 *        Anhand von Zeitmessungen werden Gegner, Animationen und Munition bewegt.
 */
class Gameloop: public QObject
{
    Q_OBJECT
public:
   Gameloop();

private:
   int previous_time;   // vorhergehende Zeit
   int current_time;    // aktuelle zeit
   int elapsed_time;    // Vergangene zeit

   int enemy_wave;   // In welchem Level befindet sich der Gegner

   // Methoden die im während der Updatefunktion aufgerufen werden
   inline void move_enemies();
   inline void move_animations();
   inline void check_enemies_finished();
   inline void check_enemies_health();
   inline void actualize_score();
   inline void tower_scan();
   inline void move_bullets();


   void spawnEnemy();
   void check_stage_cleared();     // Prüft ob das Levelende erreicht wurde (anhand der zerstörten Gegner)
   void increase_enemy_wave();     // Setzt das Level um 1 nach oben

public slots:
   /*!
    * Kernfunktion der Klasse,
    * der Slot repräsentiert den Gameloop.
    */
    void update();
};

#endif // GAMELOOP_H
