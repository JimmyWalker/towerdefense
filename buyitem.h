#ifndef BUYITEM_H
#define BUYITEM_H

#include <QPixmap>

/*!
 * \brief Die BuyItem Klasse repräsentiert die einzelnen Schaltflächen im Kaufmenü
 * beim anklicken eines Towers. Voraussetzung zum Kaufen ist es genügend Credits
 * (Bildschirm oben Links) zu besitzen.
 */
class BuyItem : public QPixmap
{
public:
    /*!
     * \brief BuyItem       - Konstruktor
     * \param name          - Bezeichnung des Icons (Buttons)
     * \param price         - Preis des Items
     * \param clicked       - Pfad für Image im geklickten Zustand
     * \param buyable       - Pfad für Image im geklickten Zustand
     * \param non_buyable   - Pfad für Image im nicht kaufbaren Zustand
     * \param pos_own       - eigene Position innerhalb des Kaufmenüs
     * \param pos_tower     - Position des Towers (Kaufmenü richtet sich danach aus)
     */
    BuyItem( QString name = "", int price = 0, QString clicked = "", QString buyable = "", QString non_buyable = "", QPoint pos_own = QPoint(), QPoint pos_tower = QPoint());

    // Getter - Methoden
        // gibt die Positions als Koordinate zurück
        QPoint get_pos();

   // Setter - Methoden
        // setzen der Stati der Items
        void set_item_clicked();
        void set_item_buyable();
        void set_item_non_buyable();

private:
   QString name;
   int price;
   QPoint pos;

   // Bildpfade für unterschiedlichen Stati
   QString item_clicked;
   QString item_buyable;
   QString item_non_buyable;

};

#endif // BUYITEM_H
