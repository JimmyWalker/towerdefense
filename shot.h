#ifndef SHOT_H
#define SHOT_H


#include <QString>
#include <QPixmap>
#include <QGraphicsPixmapItem>
#include <QObject>
#include <QGraphicsItem>
#include <QMediaPlayer>
#include <QGraphicsSceneMouseEvent>
#include <QPoint>
#include <qmath.h>


/*!
 * \brief Die Klasse Shot stellt den Türmen die Munition zur Verfügung.
 * Je nach Art des Turmes erhält die Munition andere Eigenschaften.
 */
class Shot: public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT

public:
   Shot(QGraphicsItem *parent=0, QPoint start = QPoint(0,0),QPoint aim = QPoint(0,0), QString kind = "");
   ~Shot();

   // Bezeichnung für die Art der Munition (ice, bullet, rocket)
   QString kind_of_shot;
   // Ziel Koordinaten
   QPoint aim;
   // in dem Radius nimmt ein Gegner beim aufprallen der Kugel Schaden
   int demage_radius;
   // Wert des Schadens den ein Gegner von seiner Lebenskraft abgezogen bekommt
   int demage;

   // Leg wichtige Startwerte fest
   void init();

   // bewegt die Munition
   void move();

   // Prüft ob ein Gegner von der Kugel getroffen wurde
   inline void check_demage();

private:
   // Geschwindigkeit der Munition
   int velocity;
};

#endif // SHOT_H
