#-------------------------------------------------
#
# Project created by QtCreator 2015-06-18T17:54:48
#
#-------------------------------------------------

QT       += core gui
QT       += xml
QT       += multimedia


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TowerDefense
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    game.cpp \
    waypoint.cpp \
    tower.cpp \
    animation.cpp \
    gameloop.cpp \
    enemy.cpp \
    trooper.cpp \
    jeep.cpp \
    rambo.cpp \
    logic.cpp \
    gamedata.cpp \
    player.cpp \
    score.cpp \
    buymenu.cpp \
    buyitem.cpp \
    shot.cpp \
    particle.cpp \
    particleemitter.cpp

HEADERS  += mainwindow.h \
    game.h \
    waypoint.h \
    tower.h \
    animation.h \
    gameloop.h \
    enemy.h \
    trooper.h \
    jeep.h \
    rambo.h \
    logic.h \
    gamedata.h \
    player.h \
    score.h \
    buymenu.h \
    buyitem.h \
    shot.h \
    particle.h \
    particleemitter.h

FORMS    += mainwindow.ui

RESOURCES += \
    ress.qrc
