#ifndef JEEP_H
#define JEEP_H

#include "enemy.h"

/*!
 * \brief Die Jeep Klasse, stellt eine weitere Form von Gegnern da, die von Enemy erbt.
 * Diese ist jedoch noch nicht implementiert.
 */
class Jeep: public Enemy
{
public:
    Jeep();
};

#endif // JEEP_H
