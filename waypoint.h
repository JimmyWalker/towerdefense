#ifndef WAYPOINT
#define WAYPOINT

#endif // WAYPOINT

#include <QString>
#include <QPixmap>
#include <QGraphicsPixmapItem>
#include <QObject>
#include <QGraphicsItem>
#include <QMediaPlayer>
#include <QPoint>
#include <QGraphicsScene>
#include <QDebug>
#include <QDateTime>
#include <QTimer>


/*!
 * \brief die Klasse Waypoint ist der Kern der Wegfindung aller Gegner.
 * Die Way Points ansich repräsentieren nicht jeden Straßenteil,
 * sondern lediglich die jenigen, die einen Richtungswechsel mitsich
 * bringen. (rechts, links, oben und unten). Aus diesen Abschnitten
 * erhalten die Gegner die Richtung.
 */
class Waypoint: public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT

public:
   Waypoint(QGraphicsItem *parent=0, int x = 0, int y = 0, int level = 0, QString path = "", QString direction = "", int numberOfNext = 0);
   ~Waypoint();

   int get_pos_x();
   int get_pos_y();

   QString get_direction();
   int get_numberOfNext();
   // gibt den Straßenmittelpunkt zurück
   QPoint get_centered_point();


private:
   int pos_x;
   int pos_y;
   int level;
   // Pfad der Grafikdatei
   QString path;
   // Richtung in die von diesem Teil der Straße weiter gegangen wird
   QString direction;
   // Anzahl der nächsten Straßenabschnitte ohne weitere Richtungsanweisungen
   int numberOfNext;

   // setzen der Grafik und Position des Straßenteils
   void init();

};
