#ifndef PLAYER_H
#define PLAYER_H

/*!
 * \brief Die Player Klasse ist überschaubar und fast ausschließlich Methoden
 * die für den Geldfluss notwendig sind. In diesem Sinne gibt es keinen direkt Spieler
 * der auf dem Spielfeld sichtbar ist.
 */
class Player
{
public:
    Player();

    /*!
     * \brief coins
     * Der Spieler kann mit den Münzen Türme aufbauen (Kosten sind nach Bauart geregelt).
     * Weiterhin dienen die Coins als Punktestand, je mehr desto besser.
     * Der Spieler kann sich im Laufe des Spieles durch den Abschuss von Gegner neue Coins erwirtschaften
     */
    int coins;

    // liefert die aktuelle Anzahl an Coins
    int get_coins();
    // dient als Setter-Methode zum verringern oder erhöhen der Coins
    void manipulate_coins( signed int val);
};

#endif // PLAYER_H
