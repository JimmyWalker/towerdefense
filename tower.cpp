#include "tower.h"
#include "game.h"

using namespace std;
extern Game *game;

// Zugriff auf externe konstante Werte
extern const int ICE_ATTACK_RADIUS;
extern const int ICE_SHOT_RATE;
extern const int ICE_DEMAGE;

extern const int BULLET_ATTACK_RADIUS;
extern const int BULLET_SHOT_RATE;
extern const int BULLET_DEMAGE;

extern const int ROCKET_ATTACK_RADIUS;
extern const int ROCKET_SHOT_RATE;
extern const int ROCKET_DEMAGE;

extern const int TILE_SIZE;


Tower::Tower(QGraphicsItem *parent, int x, int y, int level, QString path): QObject(), QGraphicsPixmapItem(parent), build_process_active(false), animation_counter(0), kind_of_tower(""), time_last_shot(0)
{
    this->pos_x = x;
    this->pos_y = y;
    this->level = level;
    this->path = path;

    init();
}

Tower::~Tower()
{

}

void Tower::init()
{
    // Bild und Position festlegen
    setPixmap(QPixmap(path));
    setPos(pos_x * TILE_SIZE,pos_y * TILE_SIZE);
    setZValue(level);

    time_last_shot = QDateTime::currentMSecsSinceEpoch();

    // Spriteanimationen werden in einem Container gesichert
    for(int i = 0; i <= 42; i++)
    {
        sprites_building.append(QPixmap(":/building_animation/images/building_animation/building_process" + QString::number(i) + ".png"));
    }
}



QPoint Tower::get_centered_point()
{
    int center_x = pos().x() + (pixmap().width() / 2);
    int center_y = pos().y() + (pixmap().height() / 2);
    return QPoint(center_x,center_y);
}

void Tower::start_building(QString activate_item)
{
    build_process_active = true;
    this->kind_of_tower = activate_item;
    game->list_tower_to_animate.append(this);
}

void Tower::stop_building()
{
    build_process_active = false;
    game->list_tower_to_animate.removeOne(this);
    specify_tower();

    // Wird ein Geschützturm errichtet, erfolgt der Startschuss für das Erzeugen von Gegnern
    game->start_game = true;
}

void Tower::building_animation()
{

    // Wenn keine Animationsbilder mehr vorhanden sind, wird der Bauprozess abgeschlossen
    if(animation_counter >= ( sprites_building.count() - 1 ) )
    {
        stop_building();
        //determine_start_angle();
        return;
    }

    // Tauschen der Animationsbilder
    setPixmap(sprites_building[0]);
    sprites_building.append(sprites_building[0]);
    sprites_building.removeAt(0);
    animation_counter++;
}

void Tower::specify_tower()
{
    /*
     * Legt je nach Auswahl des Turmes seine Eigenschaften fest
     */
    if(kind_of_tower == "ice")
    {
        // Gegner werden lediglich eingefroren und in
        // ihrer Bewegung verlangsamt
        this->demage = ICE_DEMAGE;
        this->attack_radius = ICE_ATTACK_RADIUS;
        this->shot_rate = ICE_SHOT_RATE;
        setPixmap(QPixmap(":/tower/images/tower/ice_tower.png"));
    }

    if(kind_of_tower == "bullet")
    {
        this->demage = BULLET_DEMAGE;
        this->attack_radius = BULLET_ATTACK_RADIUS;
        this->shot_rate = BULLET_SHOT_RATE;
        setPixmap(QPixmap(":/tower/images/tower/bullet_tower.png"));
    }

    if(kind_of_tower == "rocket")
    {
        this->demage = 33;
        this->attack_radius = ROCKET_ATTACK_RADIUS;
        this->shot_rate = ROCKET_SHOT_RATE;
        setPixmap(QPixmap(":/tower/images/tower/rocket_tower.png"));
    }
}

void Tower::scan_attack_radius()
{
    double farthest_distance = 0;
    QPoint aim;

    for (int i = 0; i < game->list_enemies.count(); i++)
    {
        double temp_distance = get_distance( game->list_enemies[i]->get_centered_point() );

        // Prüfen ob im Attackierradius und größer als bereits gemessene Entfernungen?
        if( temp_distance <= attack_radius && temp_distance > farthest_distance)
        {
           farthest_distance = temp_distance;
           aim = game->list_enemies[i]->get_centered_point();

           // Vorausschauendes schießen der Türme (Wo wird der Gegner beim Aufprall der Kugel sein?)
           if(game->list_enemies[i]->direction == "left")     aim.setX(aim.x() - 40);
           if(game->list_enemies[i]->direction == "right")    aim.setX(aim.x() + 40);
           if(game->list_enemies[i]->direction == "up")       aim.setY(aim.y() - 40);
           if(game->list_enemies[i]->direction == "down")     aim.setY(aim.y() + 40);
        }
    }
    int current_time = QDateTime::currentMSecsSinceEpoch();
    int time_dif = current_time - time_last_shot;

    // Turm sucht sich die größte Distanz zum Feuern
    if( farthest_distance != 0 && time_dif > shot_rate )shot(aim);
}

void Tower::shot(QPoint aim)
{
    time_last_shot = QDateTime::currentMSecsSinceEpoch();

    Shot *s = new Shot(0,this->get_centered_point(),aim,this->kind_of_tower);

    // Erstellung einer Linie (intern) zur Berechnung des Winkels zum Ziel
    QLineF ln(QPointF(get_centered_point().x(),get_centered_point().y()),aim);
    double angle =  -1 * ln.angle();
    s->setRotation(angle);

    // Schüsse werden in einer Liste gespeichert und in die Szene gesetzt
    game->list_shots.append(s);
    game->scene->addItem(s);
}

double Tower::get_distance(QPoint ziel)
{
   return sqrt( pow(ziel.x() - this->x(),2 ) + pow( ziel.y() - this->y(),2 )  );
}






