#include "player.h"
#include "score.h"
#include "game.h"

// Zugriff auf globales Gameobjekt
extern Game *game;

// Zugriff auf externe Konstanten
extern const int START_COINS;

/*!
 * \brief Player::Player
 *  Der Spieler erhält X Start-Coins
 */
Player::Player()
{
    coins = START_COINS;
}


int Player::get_coins()
{
    return coins;
}

void Player::manipulate_coins(signed int val)
{
    this->coins += val;
}

