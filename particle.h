#ifndef PARTICLE_H
#define PARTICLE_H

#include <QPainter>
#include <QGraphicsItem>
#include <QDebug>
#include <QPoint>
#include <qmath.h>

/*!
 * \brief Die Particle Klasse dient zur Erstellung eines einzigen Partikels
 * für ein Partikelsystem
 */
class Particle : public QGraphicsItem
{

public:
    Particle(QPoint location, double angle);
    ~Particle();

    // gibt die Grenzen des Partikels zurück
    QRectF boundingRect() const;
    // Zeichenmethode von "QGraphicsItem" wird implementiert
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

    // Manipuliert die Eigenschaften in einem Loop
    void update();

    // enthält die Angabe ob die Lebenszeit des Partikels vorbei ist
    bool isDead;


private:
    // Größe des Partikels
    int size;
    // Lebenszeit
    int life_span;
    // Gelbwert von RGB
    int yellow_val;
    // Blauwert von RGB
    int blue_val;

    // Geschwindigkeit
    double velocity;
    // Beschleunigung (abnehmend)
    double acceleration;    
    // Winkel zum Ursprung in die sich das Partikel bewegen soll
    double angle;

    // Startpunkt des Partikels
    QPoint location;

    // Zuständig für Farbwechsel
    inline void change_color();

    // bewegt das Partikel
    inline void move();

};

#endif // PARTICLE_H
