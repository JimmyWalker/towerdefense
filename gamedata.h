#ifndef GAMEDATA_H
#define GAMEDATA_H

#include <QList>
#include <QPixmap>

/*!
 * \brief Die Gamedata Klasse stellt dem Spiel verschiede feste Werten in
 *  Form von Konstanten zur Verfügung. Dies ermöglicht eine schnelle Änderbarkeit
 *  wichtiger Werte ohne diese Im Quellcode direkt suchen zu müssen.
 */
class Gamedata
{
public:
    Gamedata();

};

#endif // GAMEDATA_H
