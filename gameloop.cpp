#include "gameloop.h"
#include "game.h"


// Zugriff auf globales Gameobjekt
extern Game *game;

// Zugriff auf externe konstante Werte
extern const int ANIMATION_DELAY_TIME;
extern const int COINS_FOR_ENEMY;

Gameloop::Gameloop():
    current_time(0), elapsed_time(0), enemy_wave(1)
{
    // Initialisierung erster "vorhergender" Zeitpunkt
    previous_time = QDateTime::currentMSecsSinceEpoch();
}

void Gameloop::update()
{
    // Zeitwerte werden bei jeder Runde aktualisiert
    current_time = QDateTime::currentMSecsSinceEpoch();
    elapsed_time = current_time - previous_time;
    previous_time = current_time;

    static int animation_delay = 0;
               animation_delay += elapsed_time;

    static int spawn_delay = 0;
               spawn_delay += elapsed_time;

    check_enemies_health();
    actualize_score();
    check_enemies_finished();
    move_enemies();
    tower_scan();
    move_bullets();


    // Umgebungsanimationen werden in längeren Zeitabständen aktualisiert
    if(animation_delay >= ANIMATION_DELAY_TIME)
    {
        move_animations();
        animation_delay = 0;
    }

    /***
     *  Gegner werden in längeren Zeitabständen gespawnt
     *  wenn das Spiel gestartet wurde (Bedingung: der 1. Turm wurde errichtet)
     */
    if(spawn_delay >= 3000 && game->start_game == true)
    {
        spawnEnemy();
        spawn_delay = 0;
    }

    game->scene_update();
}

/***
 *  Folgende Methoden Arbeiten analog:
 *  Befindet sich ein Objekt in der Liste, so wird es bewegt!
 */
void Gameloop::move_enemies()
{
    if( game->list_enemies.count() > 0 )
    {
        for(int i = 0; i < game->list_enemies.count(); i++) game->list_enemies[i]->move();
    }
}


void Gameloop::move_animations()
{
    for(int i = 0; i < game->list_animations.count(); i++)game->list_animations[i]->move();

    // Bauanimationen falls vorhanden
    if(game->list_tower_to_animate.count() > 0)
    {
        for(int i = 0; i < game->list_tower_to_animate.count(); i++ )
        {
            game->list_tower_to_animate[i]->building_animation();
        }
    }
}


void Gameloop::check_enemies_finished()
{
    if(game->list_enemies.count() > 0)
    {
        for(int i = 0; i < game->list_enemies.count(); i++){

            if ( game->list_enemies[i]->finisehd )
            {
                delete game->list_enemies[i];
                game->list_enemies.removeOne(game->list_enemies[i]);
            }
        }
    }
}

void Gameloop::check_enemies_health()
{
    for(int i = 0; i < game->list_enemies.count(); i++)
    {
       if( game->list_enemies[i]->get_health() <= 0 )
       {
           delete game->list_enemies[i];
           game->player->manipulate_coins( COINS_FOR_ENEMY );
       }
    }
}


void Gameloop::spawnEnemy()
{
    /*
     * zukünftige Implementierung sieht verschiedene
     * Arten von Gegnern vor, diese erscheinen in Wellen
     */
    switch (enemy_wave) {
        case 1:
        {
            Rambo *rambo = new Rambo();
            game->scene->addItem(rambo);
            game->list_enemies.append(rambo);
            break;
        }
        default:
        {
            break;
        }
    }
}

void Gameloop::actualize_score()
{
    game->score->set_score( game->player->get_coins() );
}

void Gameloop::tower_scan()
{
    for(int i = 0; i < game->list_towers.count(); i++)game->list_towers[i]->scan_attack_radius();
}

void Gameloop::move_bullets()
{
    for(int i = 0; i < game->list_shots.count(); i++)game->list_shots[i]->move();
}

// Noch nicht implementiert, das Spiel ist derzeit endlos
void Gameloop::check_stage_cleared()
{

}

// noch nicht benutzt
void Gameloop::increase_enemy_wave()
{
    this->enemy_wave++;
}
