#ifndef GAME_H
#define GAME_H

#include <QApplication>
#include <QPixmap>
#include <QGraphicsPixmapItem>
#include <typeinfo>
#include <QProcess>
#include <QString>

#include <QtCore>
#include <QDebug>
#include <QFile>
#include <QDomDocument>
#include <QtXml/QXmlSimpleReader>
#include <QtXml/QtXml>
#include <QtCore/QFile>

#include <QObject>
#include <QGraphicsView>
#include <QWidget>
#include <QGraphicsScene>
#include <QTimer>
#include <QList>
#include <QGraphicsItem>
#include <QGraphicsTextItem>
#include <QMediaPlayer>
#include <QMouseEvent>
#include <QGraphicsRectItem>
#include <QPoint>

#include "waypoint.h"
#include "tower.h"
#include "animation.h"
#include "gameloop.h"
#include "enemy.h"
#include "player.h"
#include "score.h"
#include "buymenu.h"
#include "shot.h"
#include "particleemitter.h"
#include "buymenu.h"
#include "rambo.h"
#include "particle.h"



/*!
 * \brief Die Game-Klasse stellt die grafische Oberfläche dar und ist
 *        somit einer der Hauptbestandteile der gesamten Applikation
 *        Sie stellt essentiell wichtige Timer zur Verfügung die Einfluss
 *        auf das Spielgeschehen nehmen.
 *
 *        Das Spielfeld und alle statischen Grafiken und deren Animationen, sowie Wegpunkte
 *        werden aus einer XML Datei ausgelesen und in die Spielszene aufgetragen.
 *        Die Grafiken sind dabei Kacheln und keine großen Grafiken
 */
class Game:public QGraphicsView
{

public:
    Game();

    bool start_game;

    // Listen von Game-Elementen
    QList<Waypoint *> list_waypoints;
    QList<QGraphicsPixmapItem *> list_road;
    QList<Tower *> list_towers;
    QList<Tower *> list_tower_to_animate;
    QList<Animation *> list_animations;
    QList<Enemy *> list_enemies;
    QList<Shot *> list_shots;
    QList<ParticleEmitter *> list_emitter;

    // Spielesczene und ihre grafischen Elemente
    QGraphicsScene *scene;

    // Hintergrundelement der Credipoints
    QGraphicsPixmapItem *coins_bg;
    // Schriftzug der Credits
    Score *score;

    Player *player;

    Buymenu *menu;
    bool menuIsActive;

    // Methode zum repainten der Scene
    void scene_update();

    // Schließt das Kaufmenü
    void hide_menu();

    /*
     * Methode zur Erstellung einer Hilfslinie zwischen 2 Punkten
     * Wurde zum Debuggen während der Implemntierung genutzt
     */
    void set_debugLine(QPoint p1, QPoint p2);

    /*!
     * \brief create_particle_emitter - erstellt ein Partikelsystem
     * (wird bei den Rocket-Towern verwendet)
     * \param loc - wo sollen die Partikel erscheinen?
     */
    void create_particle_emitter(QPoint loc);


private:
    // Klasse die sich um alle Abläufe des Spielgeschens kümmert
    Gameloop *gameloop;
    // Eine Art Thread, der den Gameloop am laufen hält
    QTimer *gameloop_timer;

    // Stattet wichtige Elemente mit Startwerten aus
    void init();

    // Methoden zur Erstellung der Map und ihrer grafischen Elemente
    void create_map( QDomElement &root);
    void create_bg(QString &path, int &level);
    void create_asset(QString &path, int &pos_x, int &pos_y, int &level);
    void create_tower(QString &path, int &_x, int &pos_y, int &level);
    void create_road(QString path, int pos_x, int pos_y, int level);


    // Empfängt Mousklicks die für die Bedienung des Kaufmenüs notwendig sind
    void mousePressEvent(QMouseEvent *mevent);

    // Lässt das Kaufmenü nach klick auf einen Tower erscheinen
    void show_menu(int towerIndex);

};

#endif // GAME_H
