#include "score.h"

Score::Score(QGraphicsTextItem *parent):
    QGraphicsTextItem(parent), score(200)
{
    // Erzeugt Anzeigegrafik
    setPlainText(QString::number(score) );
    setDefaultTextColor(Qt::black);
    setFont(QFont("times", 16));
    this->setZValue(2);
    setPos(70, 10);
}

void Score::increase(int val)
{
    score += val;
    setPlainText( QString::number(score) ); // Zeichnet Coins neu
}

void Score::decrease(int val)
{
    score -= val;
    setPlainText( QString::number(score) ); // Zeichnet Coins neu
}

void Score::set_score(int val)
{
    score = val;
    setPlainText( QString::number(score) ); // Zeichnet Coins neu
}

int Score::get_score()
{
    return score;
}
