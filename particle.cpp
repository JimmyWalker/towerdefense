#include "particle.h"
#include "game.h"


// Zugriff auf globales Gameobjekt
extern Game *game;

// Zugriff auf externe Konstanten
extern const int PARTICLE_START_SIZE;
extern const int PARTICLE_LIFE_SPAN;
extern const int PARTICLE_VELOCITY;
extern const double PARTICLE_ACCELERATION;


Particle::Particle(QPoint loc, double angle): isDead(false), yellow_val(0), blue_val(0)
{
    acceleration = PARTICLE_ACCELERATION;
    velocity = PARTICLE_VELOCITY;
    size = PARTICLE_START_SIZE;
    life_span = PARTICLE_LIFE_SPAN;

    this->setPos(loc);
    this->setRotation(angle);
}

Particle::~Particle()
{

}


QRectF Particle::boundingRect() const
{
    return QRectF(0,0,size,size);
}

void Particle::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    QRectF rec = boundingRect();

    QBrush brush(QColor(255, yellow_val, blue_val, 127));

    painter->setBrush(brush);
    painter->drawEllipse(rec);
}

void Particle::update()
{
    move();
    size += 1;
    life_span -= 10;
    change_color();
}

void Particle::move()
{
    // Holen des festgelegten Winkels
    double theta = rotation();

    // Berechnung der neuen Position unter Berücksichtigung
    // der Geschwindigkeit
    double dy = velocity * qSin(qDegreesToRadians(theta));
    double dx = velocity * qCos(qDegreesToRadians(theta));

    setPos(x()+dx,y()+dy);

    // Prüfen ob die Lebenszeit abgelaufen ist
    if ( life_span <= 0) isDead = true;
}

void Particle::change_color()
{
    if(yellow_val <= 255) // Farbe von Rot in Gelb umwandeln
    {
        yellow_val +=10;
    }
    else // Wenn Gelb erreicht Blauwert erhöhen um weiss zu erzeugen
    {
        yellow_val = 255;
        blue_val = 255;
    }
}
