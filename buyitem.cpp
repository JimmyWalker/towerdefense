#include "buyitem.h"
#include <QPixmap>

BuyItem::BuyItem(QString name, int price,QString clicked, QString buyable, QString non_buyable, QPoint pos_own, QPoint pos_tower)
    : QPixmap()
{
    this->name = name;
    this->price = price;
    this->item_clicked = clicked;
    this->item_buyable = buyable;
    this->item_non_buyable = non_buyable;
    this->pos = pos_tower - pos_own;
}


QPoint BuyItem::get_pos()
{
    return pos;
}

void BuyItem::set_item_clicked()
{
    load(item_clicked.toLatin1());
}

void BuyItem::set_item_buyable()
{
    load(item_buyable.toLatin1());
}

void BuyItem::set_item_non_buyable()
{
    load(item_non_buyable.toLatin1());
}

