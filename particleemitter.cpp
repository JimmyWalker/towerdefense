#include "particleemitter.h"
#include "game.h"

// Zugriff auf globale Komponenten
extern Game *game;

ParticleEmitter::ParticleEmitter(QPoint loc)
{
    this->location = loc;
    emit_timer = new QTimer(this);
    connect(emit_timer, SIGNAL(timeout()),this,SLOT(update()));
    emit_timer->start(100);
    last_emitting_time = 0;
    wave = 0;
}

ParticleEmitter::~ParticleEmitter()
{
    game->list_emitter.removeOne(this);
}

void ParticleEmitter::update()
{
    int current_time = QDateTime::currentMSecsSinceEpoch();

    if ( (current_time - last_emitting_time) > 100 && wave <= 7)emit_particle();

    move_particle();

    check_particleLife();

    if ( wave == 7 && list_particles.count() == 0) delete this;


}

void ParticleEmitter::emit_particle()
{
    wave += 1;
    last_emitting_time = QDateTime::currentMSecsSinceEpoch();
    for( int i = 0; i < 360; i+=20)
    {
        Particle *p = new Particle(QPoint(location.x(),location.y()), i);
        p->setZValue(1);
        game->scene->addItem(p);
        list_particles.append(p);
    }
}

void ParticleEmitter::move_particle()
{

    if (list_particles.count() < 1) return;

    for( int i = 0; i < list_particles.count(); i++)
    {
        list_particles[i]->update();
    }
}

void ParticleEmitter::check_particleLife()
{
    if (list_particles.count() < 1) return;


    for( int i = 0; i < list_particles.count(); i++)
    {
        if(list_particles[i]->isDead == true)
        {
            delete list_particles[i];
            list_particles.removeOne(list_particles[i]);
        }
    }
}
