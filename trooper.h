#ifndef TROOPER_H
#define TROOPER_H

#include "enemy.h"

/*!
 * \brief Die Trooper Klasse soll für zukünftige Arbeit am Projekt eine weiter
 * Art von Gegner werden. (Derzeit noch nicht implementiert)
 */
class Trooper : public Enemy
{
public:
    Trooper();

};

#endif // TROOPER_H
