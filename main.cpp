#include "mainwindow.h"
#include "game.h"
#include <QApplication>

// Objekt des Spielfeldes
Game *game;
/*!
 * \brief main - Startpunkt des Spielgeschehens
 */
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    game = new Game();
    game->show();

    return a.exec();
}

