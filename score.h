#ifndef SCORE_H
#define SCORE_H

#include <QGraphicsTextItem>
#include <QFont>

/*!
 * \brief Die Score-Klasse händelt die Punkte
 *        des Spielers (Coins)
 */
class Score:public QGraphicsTextItem{

public:
    Score(QGraphicsTextItem *parent = 0);

    // Erhöht die Punkte des Spielers
    void increase(int val);
    // verringert die Punkte Spielers (findet derzeit noch keine Anwendung)
    void decrease(int val);

    // Getter und Setter - Methoden
    int get_score();
    void set_score(int val);

private:
    int score;      // eigentliche Punktzahl

};

#endif // SCORE_H
